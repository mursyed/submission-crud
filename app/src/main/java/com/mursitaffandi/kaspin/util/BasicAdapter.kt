package com.mursitaffandi.kaspin.util

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class BasicAdapter<T, V : ViewBinding>(private val listItem: MutableList<T>) :
    RecyclerView.Adapter<BasicAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    lateinit var res: Resources

    lateinit var layout: V

    fun setData(newData: List<T>) {
        this.listItem.clear()
        this.listItem.addAll(newData)
        notifyDataSetChanged()
    }
    abstract fun setViewBinding(layoutInflater: LayoutInflater, parent: ViewGroup): V

    abstract fun bind(item: T, view: V, position: Int)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        layout = setViewBinding(LayoutInflater.from(parent.context), parent)
        res = parent.context.resources
        return ViewHolder(layout.root)
    }

   /* override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty())
        super.onBindViewHolder(holder, position, payloads)
        else
        bind(listItem[position], layout, holder.adapterPosition, payloads)

    }*/
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       bind(listItem[position], layout, holder.adapterPosition)
   }

    override fun getItemCount() = listItem.size

}