package com.mursitaffandi.kaspin.util

const val KEY_INTENT_CHECKOUT = "id_selected_products"
const val ORDERS_COLLECTION_NAME = "kaspin-orders"
const val ORDER_CREATEDAT = "createdAt"
const val ORDER_CONTAINS = "contains"
