package com.mursitaffandi.kaspin.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.*

fun <T> Context.openActivity(it: Class<T>, extras: Bundle.() -> Unit = {}) {
    val intent = Intent(this, it)
    intent.putExtras(Bundle().apply(extras))
    startActivity(intent)
}

inline fun <reified T : Activity> Context.startActivity(block: Intent.() -> Unit = {}) {
    startActivity(Intent(this, T::class.java).apply(block))
}

inline fun <reified T : Activity> Context.startActivityFinish(block: Intent.() -> Unit = {}) {
    this.startActivity(Intent(this, T::class.java).apply(block))
    (this as Activity).finish()
}

/**
 * Don't forget to .show() manually
 * */
fun Context.toas(msg: String): Toast {
    return Toast.makeText(this, msg, Toast.LENGTH_LONG)
}

fun now(): Timestamp {
    return Timestamp(Date())
}

@SuppressLint("SimpleDateFormat")
fun convertTime(time: Timestamp) : String {
    val date = time.toDate()
    val sfd = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    return sfd.format(date)
}