package com.mursitaffandi.kaspin.util

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mursitaffandi.kaspin.di.Injection
import com.mursitaffandi.kaspin.repository.RepositoryImpl
import com.mursitaffandi.kaspin.view.checkout.CheckoutViewModel
import com.mursitaffandi.kaspin.view.order.OrderViewModel
import com.mursitaffandi.kaspin.view.product.ProductViewModel
import com.mursitaffandi.kaspin.view.transaction.TransactionViewModel

class ViewModelFactory private constructor(private val mRepository: RepositoryImpl) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
         var instance: ViewModelFactory? = null

        fun getInstance(context: Context): ViewModelFactory =
            instance ?: synchronized(this) {
                ViewModelFactory(Injection.provideRepository(context)).apply { instance = this }
            }
    }

    @SuppressWarnings("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(ProductViewModel::class.java) -> {
                ProductViewModel(mRepository) as T
            }
            modelClass.isAssignableFrom(TransactionViewModel::class.java) -> {
                TransactionViewModel(mRepository) as T
            }
            modelClass.isAssignableFrom(CheckoutViewModel::class.java) -> {
                CheckoutViewModel(mRepository) as T
            }
            modelClass.isAssignableFrom(OrderViewModel::class.java) -> {
                OrderViewModel(mRepository) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }
    }
}