package com.mursitaffandi.kaspin.util

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BasicActivity<V: ViewBinding?>:AppCompatActivity() {
    var layout: V? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layout = getViewBinding()
        setContentView(layout?.root)
    }

    abstract fun getViewBinding(): V

    override fun onDestroy() {
        super.onDestroy()
        layout = null
    }
}