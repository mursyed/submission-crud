package com.mursitaffandi.kaspin.di

import android.content.Context
import com.mursitaffandi.kaspin.repository.RepositoryImpl
import com.mursitaffandi.kaspin.repository.local.LocalDatasource
import com.mursitaffandi.kaspin.repository.local.MainDatabase
import com.mursitaffandi.kaspin.repository.remote.FireDataSource

object Injection {
    fun provideRepository(context: Context): RepositoryImpl {
        val database = MainDatabase.getInstance(context)
        val localDataSource = LocalDatasource.getInstance(database.productDao())
        val fireDataSource = FireDataSource.getInstance()
        return RepositoryImpl.getInstance( localDataSource, fireDataSource)
    }
}