package com.mursitaffandi.kaspin.model

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "product", indices = [
    Index(value = ["id", "code"], unique = true)
])
data class Product(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    var id: Int = 0,
    var code: String,
    var name: String,
    var stock: Int
    ) : Parcelable
