package com.mursitaffandi.kaspin.model

import androidx.annotation.Keep
import com.google.firebase.Timestamp
import com.google.firebase.firestore.PropertyName
import com.mursitaffandi.kaspin.util.ORDER_CONTAINS
import com.mursitaffandi.kaspin.util.ORDER_CREATEDAT

@Keep
class Order(
    @get:PropertyName(ORDER_CREATEDAT)
    @set:PropertyName(ORDER_CREATEDAT)
    var createdAt: Timestamp = Timestamp.now(),

    @get:PropertyName(ORDER_CONTAINS)
    @set:PropertyName(ORDER_CONTAINS)
    var contains: List<Int>? = listOf(),
)