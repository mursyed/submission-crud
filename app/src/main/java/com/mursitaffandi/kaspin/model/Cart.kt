package com.mursitaffandi.kaspin.model

data class Cart(
    val id: Int,
    val code : String,
    val name : String,
    val stock : Int,
    val selected : Boolean = false
)
