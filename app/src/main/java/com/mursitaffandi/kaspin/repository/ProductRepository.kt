package com.mursitaffandi.kaspin.repository

import com.mursitaffandi.kaspin.model.Product
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
     fun getAllProduct(): Flow<List<Product>>
     fun getFromCart(idsCart: List<Int>) : Flow<List<Product>>
     fun transactionProduct(ids: List<Int>)
     fun addProduct(code: String, name: String, stock: Int)
     fun updateProduct(id: Int, name : String, stock: Int)
     fun deleteProduct(id: Int)
}