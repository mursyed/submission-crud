package com.mursitaffandi.kaspin.repository

import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.mursitaffandi.kaspin.model.Order
import com.mursitaffandi.kaspin.vo.Result

interface OrderRepository {
    fun getQueryOrder() : FirestoreRecyclerOptions<Order?>
    suspend fun addOrder(order: Order) : Result<String>
    suspend fun removeOrder(idSnapshot: String)
}