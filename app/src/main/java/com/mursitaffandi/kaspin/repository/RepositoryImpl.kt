package com.mursitaffandi.kaspin.repository

import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.mursitaffandi.kaspin.model.Order
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.repository.local.LocalDatasource
import com.mursitaffandi.kaspin.repository.remote.FireDataSource
import com.mursitaffandi.kaspin.vo.Result
import kotlinx.coroutines.flow.Flow

class RepositoryImpl private constructor(
    private val localDataSource: LocalDatasource,
    private val fireDataSource: FireDataSource
) : ProductRepository, OrderRepository {
    companion object {
        @Volatile
        private var instance: RepositoryImpl? = null

        fun getInstance(localData: LocalDatasource, fireData: FireDataSource): RepositoryImpl =
            instance ?: synchronized(this) {
                RepositoryImpl(localData, fireData).apply {
                    instance = this
                }
            }
    }

    override fun getAllProduct(): Flow<List<Product>> = localDataSource.getAllProduct()

    override fun getFromCart(idsCart: List<Int>): Flow<List<Product>> {
        return localDataSource.getFromCart(idsCart)
    }

    override fun transactionProduct(ids: List<Int>) {
        return localDataSource.transactionProduct(ids)
    }

    override fun addProduct(code: String, name: String, stock: Int) {
        localDataSource.addProduct(Product(code = code, name = name, stock = stock))
    }

    override fun updateProduct(id: Int, name: String, stock: Int) {
        localDataSource.editProduct(id, name, stock)
    }

    override fun deleteProduct(id: Int) {
        localDataSource.deleteProduct(id)
    }

    override fun getQueryOrder(): FirestoreRecyclerOptions<Order?> = fireDataSource.getAllOrders()


    override suspend fun addOrder(order: Order) : Result<String> {
        return fireDataSource.addOrder(order)
    }

    override suspend fun removeOrder(idSnapshot: String) = fireDataSource.deleteOrder(idSnapshot)
}