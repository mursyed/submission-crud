package com.mursitaffandi.kaspin.repository.local

import androidx.room.*
import com.mursitaffandi.kaspin.model.Product
import kotlinx.coroutines.flow.Flow


@Dao
interface ProductDao {
    @Query("SELECT * FROM product")
    fun getAllProduct(): Flow<List<Product>>

    @Query("SELECT * FROM product WHERE id IN (:ids)")
    fun getCustomProduct(ids:List<Int>): Flow<List<Product>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addProduct(data: Product)

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun update(datum: Product)

    @Delete
    fun delete(datum: Product)

    @Query("UPDATE product SET name = :newName,stock = :newStock WHERE id = :id")
    fun updateProduct(id:Int, newName:String, newStock:Int)

    @Query("UPDATE product SET stock = stock - 1 WHERE id IN (:ids) AND stock > 0")
    fun transactionProduct(ids:List<Int>)

    @Query("DELETE FROM product WHERE id = :id")
     fun deleteProduct(id:Int)
}
