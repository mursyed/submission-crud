package com.mursitaffandi.kaspin.repository.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mursitaffandi.kaspin.model.Product

@Database(entities = [Product::class],
    version = 1,
    exportSchema = false)
abstract class MainDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao

    companion object {

        @Volatile
        private var INSTANCE: MainDatabase? = null

        fun getInstance(context: Context): MainDatabase =
            INSTANCE ?: synchronized(this) {
                Room.databaseBuilder(
                    context.applicationContext,
                    MainDatabase::class.java,
                    "main.db"
                ).build().apply {
                    INSTANCE = this
                }
            }
    }
}