package com.mursitaffandi.kaspin.repository.remote

import android.util.Log
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.mursitaffandi.kaspin.model.Order
import com.mursitaffandi.kaspin.util.ORDERS_COLLECTION_NAME
import com.mursitaffandi.kaspin.util.ORDER_CREATEDAT
import com.mursitaffandi.kaspin.util.await
import com.mursitaffandi.kaspin.vo.Result

class FireDataSource {
    companion object {
        private var INSTANCE: FireDataSource? = null

        fun getInstance(): FireDataSource =
            INSTANCE ?: FireDataSource().apply { INSTANCE = this }
    }

    private val firestore = Firebase.firestore
    private val orders = firestore.collection(ORDERS_COLLECTION_NAME)

    fun getAllOrders(): FirestoreRecyclerOptions<Order?> {
        return FirestoreRecyclerOptions.Builder<Order>()
            .setQuery(
                orders.orderBy(ORDER_CREATEDAT, Query.Direction.ASCENDING),
                Order::class.java
            ).build()
    }

    fun deleteOrder(idSnapshot: String) {
         orders.document(idSnapshot)
            .delete()
             .addOnSuccessListener { Log.d("deleteOrder", "Document $idSnapshot successfully deleted!") }
             .addOnFailureListener { e -> Log.w("deleteOrder", "Error deleting $idSnapshot document", e) }

    }

    suspend fun addOrder(order: Order) : Result<String> {
        return try {
            when (val members = orders.add(order).await()) {
                is Result.Success -> {
                    Result.Success(members.data.id)
                }
                is Result.Error -> {
                    Result.Error(members.exception)

                }
                is Result.Canceled -> {
                    Result.Canceled(members.exception)
                }
            }
        } catch (e: Exception) {
            Result.Error(e)
        }

    }
}