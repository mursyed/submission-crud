package com.mursitaffandi.kaspin.repository.local

import com.mursitaffandi.kaspin.model.Product
import kotlinx.coroutines.flow.Flow

class LocalDatasource private constructor(private val mProductDao: ProductDao) {
    companion object {
        private var INSTANCE: LocalDatasource? = null

        fun getInstance(productDao: ProductDao): LocalDatasource =
            INSTANCE ?: LocalDatasource(productDao).apply { INSTANCE = this }
    }

    fun getAllProduct(): Flow<List<Product>> = mProductDao.getAllProduct()
    fun getFromCart(ids: List<Int>) = mProductDao.getCustomProduct(ids)
    fun transactionProduct(ids: List<Int>) = mProductDao.transactionProduct(ids)
    fun addProduct(product: Product) = mProductDao.addProduct(product)
    fun editProduct(id: Int, newName: String, newStock: Int) =
        mProductDao.updateProduct(id, newName, newStock)
    fun deleteProduct(id: Int) = mProductDao.deleteProduct(id)
}