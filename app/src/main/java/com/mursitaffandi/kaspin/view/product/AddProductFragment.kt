package com.mursitaffandi.kaspin.view.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mursitaffandi.kaspin.databinding.FragmentAddProductBinding
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.util.ViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PRODUCT = "edit_product"
/**
 * A simple [Fragment] subclass.
 * Use the [AddProductFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddProductFragment : DialogFragment() {
    // TODO: Rename and change types of parameters
    private var product: Product? = null
    private lateinit var binding: FragmentAddProductBinding
    private lateinit var viewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getParcelable(ARG_PRODUCT)
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAddProductBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this, ViewModelFactory.getInstance(requireContext()))[ProductViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val edtCode = binding.tilCodeProduct.editText
        val edtName = binding.tilNameProduct.editText
        val edtStock = binding.tilStockProduct.editText

        product?.let {
            edtCode?.setText(it.code)
            edtCode?.isEnabled = false
            edtName?.setText(it.name)
            edtStock?.setText(it.stock.toString())
        }

        binding.btnSaveProduct.setOnClickListener {
            val newStock = edtStock?.text.toString().toInt()
            val newName = edtName?.text.toString()
            val newCode = edtCode?.text.toString()
            if (newStock < 1 || newName.isEmpty() || newCode.isEmpty()) {
                return@setOnClickListener
            } else {
                product?.let {
                    viewModel.update(it.id, newName, newStock)
                    dismiss()
                    return@setOnClickListener
                }
                viewModel.addProduct(newCode,newName,newStock)
                dismiss()
            }
        }


    }
    companion object {
        const val TAG = "product_dialog"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddProductFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(p: Product?) =
            AddProductFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PRODUCT, p)
                }
            }
    }
}