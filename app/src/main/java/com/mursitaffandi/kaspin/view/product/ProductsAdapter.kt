package com.mursitaffandi.kaspin.view.product

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mursitaffandi.kaspin.R
import com.mursitaffandi.kaspin.databinding.ItemProductBinding
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.util.BasicAdapter

class ProductsAdapter(private val action: ProductAction, products: MutableList<Product> = mutableListOf()) : BasicAdapter<Product, ItemProductBinding>(products) {

    override fun setViewBinding(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ItemProductBinding {
        return ItemProductBinding.inflate(layoutInflater,parent,false)
    }

    override fun bind(item: Product, view: ItemProductBinding, position: Int) {
        view.apply {
            tvProductName.text = item.name
            tvProductId.text = String.format(res.getString(R.string.product_code), item.code, item.id)
            tvProductStock.text = res.getString(R.string.stock, item.stock)
            btnEdit.setOnClickListener {
                action.edit(item)
            }
            btnDelete.setOnClickListener {
                action.delete(item.id)
            }
        }
    }
}

interface ProductAction{
    fun edit(product: Product)
    fun delete(id: Int)
}