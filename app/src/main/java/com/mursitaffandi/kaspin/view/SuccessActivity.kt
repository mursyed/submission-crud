package com.mursitaffandi.kaspin.view

import android.os.Bundle
import com.mursitaffandi.kaspin.databinding.ActivitySuccessBinding
import com.mursitaffandi.kaspin.util.BasicActivity
import com.mursitaffandi.kaspin.util.startActivityFinish
import com.mursitaffandi.kaspin.view.transaction.TransactionActivity

class SuccessActivity : BasicActivity<ActivitySuccessBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layout?.apply {
            btnToHome.setOnClickListener {
                startActivityFinish<MainActivity>()
            }
            btnToTransaction.setOnClickListener {
                startActivityFinish<TransactionActivity>()
            }
        }
    }

    override fun getViewBinding(): ActivitySuccessBinding {
        return ActivitySuccessBinding.inflate(layoutInflater)
    }
}