package com.mursitaffandi.kaspin.view.order

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mursitaffandi.kaspin.repository.RepositoryImpl
import kotlinx.coroutines.launch

class OrderViewModel(private val mRepository : RepositoryImpl): ViewModel() {
    fun delete(a: String) {
        viewModelScope.launch {
            mRepository.removeOrder(a)
        }
    }

    val query = mRepository.getQueryOrder()

}