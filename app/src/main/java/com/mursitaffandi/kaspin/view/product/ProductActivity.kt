package com.mursitaffandi.kaspin.view.product

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mursitaffandi.kaspin.databinding.ActivityProductBinding
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.util.BasicActivity
import com.mursitaffandi.kaspin.util.ViewModelFactory

class ProductActivity : BasicActivity<ActivityProductBinding>(), ProductAction {
    private lateinit var viewModel: ProductViewModel
    private lateinit var productsAdapter: ProductsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[ProductViewModel::class.java]

        productsAdapter = ProductsAdapter(this)
        layout?.apply {
            btnAddProduct.setOnClickListener {
                dialogProduct()
            }
            rvProduct.apply {
                layoutManager = LinearLayoutManager(this@ProductActivity)
                setHasFixedSize(true)
            }
        }
        viewModel.products.observe(this, { p ->
            productsAdapter.setData(p)
            layout?.rvProduct?.adapter = productsAdapter
        })
    }

    override fun getViewBinding(): ActivityProductBinding {
        return ActivityProductBinding.inflate(layoutInflater)
    }

    private fun dialogProduct(product: Product? = null) {
        AddProductFragment.newInstance(product).show(supportFragmentManager, AddProductFragment.TAG)
    }

    override fun edit(product: Product) {
        dialogProduct(product)
    }

    override fun delete(id: Int) {
        viewModel.removeProduct(id)
    }
}