package com.mursitaffandi.kaspin.view.order

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mursitaffandi.kaspin.databinding.ActivityOrdersBinding
import com.mursitaffandi.kaspin.model.Order
import com.mursitaffandi.kaspin.util.BasicActivity
import com.mursitaffandi.kaspin.util.KEY_INTENT_CHECKOUT
import com.mursitaffandi.kaspin.util.ViewModelFactory
import com.mursitaffandi.kaspin.util.openActivity
import com.mursitaffandi.kaspin.view.checkout.CheckoutActivity

class OrdersActivity : BasicActivity<ActivityOrdersBinding>(), OrderAction {
    private lateinit var viewModel: OrderViewModel
    private lateinit var orderAdapter: OrderAdapter

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[OrderViewModel::class.java]

        orderAdapter = OrderAdapter(this, viewModel.query)

        layout?.apply {
            rvOrder.apply {
                layoutManager = LinearLayoutManager(this@OrdersActivity)
                setHasFixedSize(true)
                adapter = orderAdapter
            }
        }
        orderAdapter.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        orderAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        orderAdapter.stopListening()
    }


    override fun getViewBinding(): ActivityOrdersBinding {
        return ActivityOrdersBinding.inflate(layoutInflater)
    }

    override fun deleteOrder(a: String) {
        viewModel.delete(a)
    }

    override fun loadOrder(a: Order) {
        openActivity(CheckoutActivity::class.java) {
            putIntegerArrayList(KEY_INTENT_CHECKOUT, a.contains?.toCollection(ArrayList()))
        }
        finish()
    }
}