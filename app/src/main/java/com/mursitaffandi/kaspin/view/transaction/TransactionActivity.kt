package com.mursitaffandi.kaspin.view.transaction

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mursitaffandi.kaspin.R
import com.mursitaffandi.kaspin.databinding.ActivityTransactionBinding
import com.mursitaffandi.kaspin.util.*
import com.mursitaffandi.kaspin.view.checkout.CheckoutActivity
import com.mursitaffandi.kaspin.view.order.OrdersActivity

class TransactionActivity : BasicActivity<ActivityTransactionBinding>(), TransactionAction {

    private lateinit var viewModel: TransactionViewModel
    private lateinit var transactionAdapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[TransactionViewModel::class.java]
        transactionAdapter = TransactionAdapter(this)

        layout?.apply {
            rvTransaction.apply {
                layoutManager = LinearLayoutManager(this@TransactionActivity)
                setHasFixedSize(true)
                layout?.rvTransaction?.adapter = transactionAdapter
            }

            extefabCheckout.setOnClickListener {
                openActivity(CheckoutActivity::class.java) {
                    putIntegerArrayList(KEY_INTENT_CHECKOUT, viewModel.selectedItems.value)
                }
            }
            btnToOrders.setOnClickListener {
                startActivity<OrdersActivity>()
            }
        }

        viewModel.selectedItems.observe(this, {
            transactionAdapter.updateSelectedId(it)
            layout?.extefabCheckout?.apply {
                isClickable = it.size>0
                text = resources.getString(R.string.button_checkout, it.size)
            }
        })

        viewModel.products.observe(this, {
            transactionAdapter.setData(it)
        })
    }

    override fun getViewBinding(): ActivityTransactionBinding {
        return ActivityTransactionBinding.inflate(layoutInflater)
    }

    override fun onRemoveProduct(pd: Int, position: Int) {
        viewModel.selectingProduct(pd, false)
        transactionAdapter.notifyItemChanged(position)
    }

    override fun onAddProduct(pd: Int, position: Int) {
        viewModel.selectingProduct(pd, true)
        transactionAdapter.notifyItemChanged(position)
    }
}