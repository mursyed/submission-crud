package com.mursitaffandi.kaspin.view.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.repository.RepositoryImpl

class TransactionViewModel (repositoryImpl: RepositoryImpl) : ViewModel() {

    val selectedItems = MutableLiveData<ArrayList<Int>>()

    init {
        selectedItems.value = arrayListOf()
    }

    val products: LiveData<List<Product>> = repositoryImpl.getAllProduct().asLiveData()

    fun selectingProduct(id: Int, isSelected : Boolean){
        if (isSelected){
            selectedItems.value?.add(id)
        } else {
            selectedItems.value?.remove(id)
        }
        selectedItems.postValue(selectedItems.value)
    }
}
