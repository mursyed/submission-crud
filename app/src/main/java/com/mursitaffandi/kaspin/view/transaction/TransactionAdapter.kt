package com.mursitaffandi.kaspin.view.transaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mursitaffandi.kaspin.R
import com.mursitaffandi.kaspin.databinding.ItemCartBinding
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.util.BasicAdapter

class TransactionAdapter(private val action : TransactionAction, a : MutableList<Product> = mutableListOf()) : BasicAdapter<Product, ItemCartBinding>(a) {
    override fun setViewBinding(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ItemCartBinding {
        return ItemCartBinding.inflate(layoutInflater,parent,false)
    }
    private val selectedIds  = arrayListOf<Int>()

    fun updateSelectedId(newSelectedIds : ArrayList<Int>){
        selectedIds.clear()
        selectedIds.addAll(newSelectedIds)
    }

    override fun bind(item: Product, view: ItemCartBinding, position: Int) {
        view.apply {
            tvNameProductCart.text = item.name
            tvIdProductCart.text = String.format(res.getString(R.string.product_code), item.code, item.id)
            btnActionAdd.visibility = if (item.stock > 0)
                    View.VISIBLE
                else
                    View.INVISIBLE

            tvQuantityCart.text = if (item.stock > 0)
                res.getString(R.string.stock , item.stock)
            else
                res.getString(R.string.out_of_stock)

            if (selectedIds.contains(item.id)) {
                btnActionAdd.visibility = View.GONE
                btnActionRemove.visibility = View.VISIBLE
            } else {
                btnActionAdd.visibility = View.GONE
                btnActionRemove.visibility = View.VISIBLE
            }

            btnActionAdd.setOnClickListener {
                action.onAddProduct(item.id, position)
            }

            btnActionRemove.setOnClickListener {
                action.onRemoveProduct(item.id, position)
            }
        }
    }
}

interface TransactionAction{
    fun onRemoveProduct(pd: Int, position: Int)
    fun onAddProduct(pd: Int, position: Int)
}