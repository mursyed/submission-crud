package com.mursitaffandi.kaspin.view.checkout

import androidx.lifecycle.*
import com.mursitaffandi.kaspin.model.Order
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.repository.RepositoryImpl
import com.mursitaffandi.kaspin.vo.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CheckoutViewModel(private val mRepository: RepositoryImpl) : ViewModel() {
    val selectedItems = MutableLiveData<ArrayList<Int>>()
    init {
        selectedItems.value = arrayListOf()
    }
    fun removeItem(id: Int) {
        selectedItems.value?.remove(id)
        selectedItems.postValue(selectedItems.value)
    }

    fun productCart(ids: List<Int>): LiveData<List<Product>> {
        return mRepository.getFromCart(ids).asLiveData()
    }

    fun transaction() {
        selectedItems.value?.let {
            if(it.size>0)
            viewModelScope.launch(Dispatchers.IO) {
                mRepository.transactionProduct(it)
            }
        }


    }

    fun saveToOrder() {
        selectedItems.value?.let {
            if(it.size>0)
                viewModelScope.launch {
                    when(val idNewOrder = mRepository.addOrder(Order(contains = it))){
                       is Result.Success -> {println(idNewOrder)}
                       is Result.Error -> {}
                       is Result.Canceled -> {}
                    }
                }
        }
    }
}