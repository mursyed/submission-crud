package com.mursitaffandi.kaspin.view

import android.os.Bundle
import com.mursitaffandi.kaspin.databinding.ActivityMainBinding
import com.mursitaffandi.kaspin.util.BasicActivity
import com.mursitaffandi.kaspin.util.startActivity
import com.mursitaffandi.kaspin.util.startActivityFinish
import com.mursitaffandi.kaspin.view.product.ProductActivity
import com.mursitaffandi.kaspin.view.transaction.TransactionActivity

class MainActivity : BasicActivity<ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layout?.btnMenuProduct?.setOnClickListener {
            //            TODO : "balikin ke startActivity"
            startActivity<ProductActivity>()
        }

        layout?.btnMenuTransaction?.setOnClickListener {
//            TODO : "balikin ke startActivity"
            startActivity<TransactionActivity>()
        }
    }

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }
}