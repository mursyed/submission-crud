package com.mursitaffandi.kaspin.view.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.repository.RepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductViewModel(private val repositoryImpl: RepositoryImpl) : ViewModel() {

    val products: LiveData<List<Product>> = repositoryImpl.getAllProduct().asLiveData()


    fun addProduct(code: String, name: String, stock: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryImpl.addProduct(code, name, stock)
        }
    }

    fun removeProduct(id : Int){
        viewModelScope.launch(Dispatchers.IO) {
            repositoryImpl.deleteProduct(id)
        }
    }

    fun update(id: Int, name: String, stock: Int){
        viewModelScope.launch(Dispatchers.IO) {
            repositoryImpl.updateProduct(id, name, stock)
        }
    }
}


