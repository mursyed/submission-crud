package com.mursitaffandi.kaspin.view.checkout

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mursitaffandi.kaspin.databinding.ActivityCheckoutBinding
import com.mursitaffandi.kaspin.util.*
import com.mursitaffandi.kaspin.view.SuccessActivity

class CheckoutActivity : BasicActivity<ActivityCheckoutBinding>(), CheckoutAction {
    private lateinit var viewModel: CheckoutViewModel
    private lateinit var checkoutAdapter: CheckoutAdapter
    private var rotated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[CheckoutViewModel::class.java]

        checkoutAdapter = CheckoutAdapter(this@CheckoutActivity)
        rotated = savedInstanceState?.getBoolean("oriented", false) == true

        if (!rotated)
        intent?.getIntegerArrayListExtra(KEY_INTENT_CHECKOUT)?.toList()?.toCollection(ArrayList())?.let {
            viewModel.selectedItems.value = it
        }

        layout?.apply {
            rvCheckout.apply {
                layoutManager = LinearLayoutManager(this@CheckoutActivity)
                setHasFixedSize(true)
            }

            fabSave.setOnClickListener {
                viewModel.saveToOrder()
                it.visibility = View.GONE
            }

            exfabSubmit.setOnClickListener {
                viewModel.transaction()
                startActivity<SuccessActivity> {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
            }
        }
        viewModel.selectedItems.observe(this, {
            layout?.exfabSubmit?.isClickable = it.size>0
            viewModel.productCart(it).observe(this, { sp ->
                checkoutAdapter.setData(sp)
                layout?.rvCheckout?.adapter = checkoutAdapter
            })
        })

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("oriented", true)
    }

    override fun getViewBinding(): ActivityCheckoutBinding {
        return ActivityCheckoutBinding.inflate(layoutInflater)
    }

    override fun onRemoveItem(a: Int) {
        viewModel.removeItem(a)
    }
}