package com.mursitaffandi.kaspin.view.checkout

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mursitaffandi.kaspin.R
import com.mursitaffandi.kaspin.databinding.ItemTransactionBinding
import com.mursitaffandi.kaspin.model.Product
import com.mursitaffandi.kaspin.util.BasicAdapter

class CheckoutAdapter(private val action : CheckoutAction, a : MutableList<Product> = mutableListOf()) : BasicAdapter<Product, ItemTransactionBinding>(a) {
    override fun setViewBinding(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): ItemTransactionBinding {
        return ItemTransactionBinding.inflate(layoutInflater,parent,false)
    }
    override fun bind(item: Product, view: ItemTransactionBinding, position: Int) {
        view.apply {
            tvNameProduct.text = item.name
            tvIdProduct.text = String.format(res.getString(R.string.product_code), item.code, item.id)
            btnAction.text = res.getString(R.string.btn_transaction_remove)
            tvQuantity.text = if (item.stock > 0) res.getString(R.string.label_quantity) else res.getString(R.string.out_of_stock)
            btnAction.setOnClickListener {
                action.onRemoveItem(item.id)
            }
        }
    }
}

interface CheckoutAction{
    fun onRemoveItem(a: Int)
}