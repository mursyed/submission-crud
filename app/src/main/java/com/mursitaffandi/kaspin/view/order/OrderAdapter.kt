package com.mursitaffandi.kaspin.view.order

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.mursitaffandi.kaspin.databinding.ItemOrderBinding
import com.mursitaffandi.kaspin.model.Order
import com.mursitaffandi.kaspin.util.convertTime

class OrderAdapter(private val action : OrderAction, options: FirestoreRecyclerOptions<Order?>,) :
    FirestoreRecyclerAdapter<Order, OrderAdapter.OrderHolder>(options) {
    inner class OrderHolder (val binding: ItemOrderBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderAdapter.OrderHolder {
        val binding = ItemOrderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OrderHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderHolder, position: Int, model: Order) {
        holder.binding.apply {
            tvOrderName.text = convertTime(model.createdAt)
            tvOrderId.text = snapshots.getSnapshot(position).id
            btnRemove.setOnClickListener {
                action.deleteOrder(snapshots.getSnapshot(position).id)
            }

            btnLoad.setOnClickListener {
                action.loadOrder(model)
            }
        }
    }
}

interface OrderAction{
    fun deleteOrder(a: String)
    fun loadOrder(a: Order)
}